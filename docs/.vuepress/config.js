module.exports = {
    theme: 'reco',
    title: '郭小壮的博客',  // 设置网站标题
    description: '郭小壮的博客',
    base: '/xiaozhuangboke/',
    themeConfig: {
        record: '豫ICP备2020030639号',
        recordLink: 'https://beian.miit.gov.cn',
        author: '郭小壮',
        authorAvatar: '/favicon.ico',
        type: 'blog',
        subSidebar: 'auto',
        serviceWorker: true,

        valineConfig: {
            appId: 'fh9OfTm0QxuTUhDMq7ey3y0c-gzGzoHsz',// your appId
            appKey: 'KtHDgSvNAYVYKPi8nc2HJCIr', // your appKey
        },
        nav: [
            {text: '文章', link: '/article/', icon: 'reco-blog'},
            {
                text: '资源',
                items: [
                    {text: '后端', link: '/share/后端/'},
                    {text: '前端', link: '/share/前端/'},
                    {text: 'unity', link: '/share/unity/'},
                    {text: '其他', link: '/share/其他/'}
                ]
            },
            {text: 'github', link: 'https://github.com/guowenzhuang', icon: 'reco-github'},

        ],
        sidebar: {
            '/article/': [
                {
                    title: 'java',
                    collapsable: false,
                    children: [
                        '/article/java/HashMap',
                        '/article/java/bingfa',
                        '/article/java/cglib',
                        '/article/java/dongtaidaili',
                        '/article/java/flutterrumen',
                        '/article/java/javaBase',
                        '/article/java/javazhentihuizong',
                        '/article/java/jdk8',
                        '/article/java/shenruqianchu',

                    ]
                }, {
                    title: '随笔',
                    collapsable: false,
                    children: [
                    ]
                }, {
                    title: '如何学习&如何搜索',
                    collapsable: false,
                    children: [
                        '/article/learn/HowLearn'
                    ]
                }
            ]
        },
        sidebarDepth: 1,
        blogConfig: {
            category: {
                location: 2,     // 在导航栏菜单中所占的位置，默认2
                text: 'Category' // 默认文案 “分类”
            },
            tag: {
                location: 3,     // 在导航栏菜单中所占的位置，默认3
                text: 'Tag'      // 默认文案 “标签”
            }
        }
    },
    // pwa配置
    /*    head: [
            ['link', { rel: 'icon', href: '/logo.png' }],
            ['link', { rel: 'manifest', href: '/Manifest.json' }],
            ['meta', { name: 'theme-color', content: '#3eaf7c' }],
            ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
            ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
            ['link', { rel: 'apple-touch-icon', href: '/xiaozhuangboke/icons/favicon-256.png' }],
            ['link', { rel: 'mask-icon', href: '/xiaozhuangboke/icons/logo.png', color: '#3eaf7c' }],
            ['meta', { name: 'msapplication-TileImage', content: '/xiaozhuangboke/icons/favicon-128.png' }],
            ['meta', { name: 'msapplication-TileColor', content: '#000000' }]
        ],
        plugins: ['@vuepress/pwa', {
            serviceWorker: true,
            updatePopup: true
        }],*/
}
