---
home: true
heroImage: /favicon.ico
heroText:  郭小壮的博客
actionText: 开始 →
actionLink: /article/
tagline: 技术分享,学习方法分享
features:
- title: Java
  details: spring,cloud
- title: 前端
  details: html,css,js
- title: 学习
  details: 如何学习
---
