---
title: Flutter入门
date: 2020-10-06
categories:
 - 前端
tags:
 - Flutter
---
# Flutter入门

## Flutter安装(Windows)

1. 下载Flutter解压包

    [点我下载](https://flutter.dev/docs/development/tools/sdk/releases#windows)
    然后解压到目录中 作者的目录是 D:\\flutter
2. 配置环境变量

         PUB_HOSTED_URL=https://pub.flutter-io.cn
         FLUTTER_STORAGE_BASE_URL=https://storage.flutter-io.cn
         PATH 新增 D:\\flutter\bin   # 换成你的解压目录 记得用;号分开
3. 安装依赖项
    打开cmd/powershell 输入  
    
        flutter doctor    
    稍等片刻 会出现如下显示
    ![flutterdockor图](/boke/images/flutter/flutter_doctor.png)
    
        继续输入命令:
        
        flutter doctor --android-licenses  # 出现提示 一直输入y
4. IDEA/Android Studio 安装插件
     
     * Flutter插件： 支持Flutter开发工作流 (运行、调试、热重载等).
     * Dart插件： 提供代码分析 (输入代码时进行验证、代码补全等).
5. 手机连接电脑 
    手机连接上电脑 打开开发者模式 开启USB调试 & 允许USB安装应用
## 运行第一个Flutter项目(IDEA)
1. 新创建个Flutter项目 选择好刚开始解压的flutter目录
	![fluttercreate1](/boke/images/flutter/createflutter1.png)
	项目名称:hello_flutter
	![fluttercreate2](/boke/images/flutter/createflutter2.png)
	新建完成
	![fluttercreate3](/boke/images/flutter/createflutter3.png)
2. 修改gradle配置(gradle国内访问不了)
	D:\flutter\packages\flutter_tools\gradle\flutter.gradle 
	把google() 和 jcenter() 注释掉。下面添加
		
        maven { url 'https://maven.aliyun.com/repository/google' }
        maven { url 'https://maven.aliyun.com/repository/jcenter' }
        maven { url 'http://maven.aliyun.com/nexus/content/groups/public' }
        
    再打开当前项目文件夹/android/build.gradle文件
       	把google() 和 jcenter() 注释掉。(此文件下有两处都需要替换 buildscript下和allprojects下)下面添加
       		
               maven { url 'https://maven.aliyun.com/repository/google' }
               maven { url 'https://maven.aliyun.com/repository/jcenter' }
               maven { url 'http://maven.aliyun.com/nexus/content/groups/public' }   
               
	![fluttercreate6](/boke/images/flutter/createflutter6.png)
    
                     
3. 修改gradle下载地址
    [下载gradle-4.10.2-all.zip](http://services.gradle.org/distributions/)
   找到项目中的android->gradle->wrapper->:
   把解压包放到此目录下 不要解压
   修改同级目录下的文件:gradle-wrapper.properties
   把distributionUrl修改为:
   
        distributionUrl=gradle-4.10.2-all.zip

3. 修改启动参数 (白屏问题)
    ![fluttercreate5](/boke/images/flutter/createflutter5.png)
5. 启动项目
	点击右上角 启动按钮 出现如图
    ![fluttercreate7](/boke/images/flutter/createflutter7.png)
    此时手机上会弹出是否安装应用 
    安装应用后打开即可 效果图如下
    ![fluttercreate8](/boke/images/flutter/createflutter8.jpg)
    
    完成


		

      
              
            
         