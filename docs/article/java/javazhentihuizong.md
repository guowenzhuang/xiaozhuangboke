---
title: Java真题汇总
date: 2020-10-06
categories:
 - 后端
tags:
 - java
---
# Java真题汇总

## 📝目录
- [java语言特性与设计模式](#javatexing)
- [深入浅出JVM](#jvm)

## java语言特性与设计模式 <a name = 'javatexing'> </a>

### 进程与线程的区别与联系

1. 调度：线程是独立调度的基本单位，进程是拥有资源的基本单位。在同一进程中，线程的切换不会引起进程的切换；在不同的进程中，进行线程切换，则会引起进程的切换。 

2. 拥有资源：进程是拥有资源的基本单位，线程不拥有资源，但线程可以共享器隶属进程的系统资源。 

3. 并发性：进程可以并发执行，而且同一进程内的多个线程也可以并发执行，大大提高了系统的吞吐量。 

4. 系统开销：创建和撤销进程时，系统都要为之分配或回收资源，在进程切换时，涉及当前执行进程CPU环境的保存以及新调度的进程CPU环境的设置；而线程切换时只需保存和设置少量寄存器内容，因此开销很小，另外，由于同一进程内的多个线程共享进程的地址空间，因此这些线程之间的同步与通信比较容易实现，甚至无须操作系统的干预。 

5. 通信方面：进程间通信需要借助操作系统，而线程间可以直接读/写进程数据段来进行通信。

相关链接: [https://blog.csdn.net/chen134225/article/details/82967718](https://blog.csdn.net/chen134225/article/details/82967718)

### 简单介绍一下进程的切换过程

1. 切换页目录以使用新的地址空间

2. 切换内核栈和硬件上下文

切换会保存寄存器,栈等线程相关的现场,需要由用户态切换到内核态

相关链接: [https://blog.csdn.net/c_121/article/details/80512999](https://blog.csdn.net/c_121/article/details/80512999)

### 你经常使用哪些Linux命令,主要用来解决什么问题

1. awk  一种处理文本文件的语言，是一个强大的文本分析工具   
   
   相关链接: [https://www.runoob.com/linux/linux-comm-awk.html](https://www.runoob.com/linux/linux-comm-awk.html)

2. top 提供运行系统的动态实时视图 
   
   相关链接: [https://www.runoob.com/linux/linux-comm-top.html](https://www.runoob.com/linux/linux-comm-top.html)

3. netstat 用于显示网络状态 
   
   相关链接: [https://www.runoob.com/linux/linux-comm-netstat.html](https://www.runoob.com/linux/linux-comm-netstat.html)

4. grep 用于查找文件里符合条件的字符串 
   
   相关链接: [https://www.runoob.com/linux/linux-comm-grep.html](https://www.runoob.com/linux/linux-comm-grep.html)

5. less 浏览文件 
   
   相关链接: [https://www.runoob.com/linux/linux-comm-less.html](https://www.runoob.com/linux/linux-comm-less.html)

6. tail 查看文件的内容 
   
   相关链接: [https://www.runoob.com/linux/linux-comm-tail.html](https://www.runoob.com/linux/linux-comm-tail.html)

7. Linux中cat、more、less、tail、head命令的区别 
   
   相关链接: [https://www.cnblogs.com/losbyday/p/5856106.html](https://www.cnblogs.com/losbyday/p/5856106.html)

### 为什么TCP建连需要3次握手而断连需要4次

1. TCP 三次握手 
   
   相关链接: [https://blog.csdn.net/laladebon/article/details/82350989](https://blog.csdn.net/laladebon/article/details/82350989)

2. TCP 四次挥手 
   
   相关链接: [https://baike.baidu.com/item/tcp%E5%9B%9B%E6%AC%A1%E6%8C%A5%E6%89%8B/7922126](https://baike.baidu.com/item/tcp%E5%9B%9B%E6%AC%A1%E6%8C%A5%E6%89%8B/7922126)

### 为什么TCP关闭连接时需要TIME_WAIT状态为什么要等2MSL

1. 为了保证A发送的最后一个ACK报文能够到达B。这个ACK报文段有可能丢失，因而使处在LAST-ACK状态的B收不到对已发送的FIN+ACK报文段的确认。B会超时重传这个FIN+ACK报文段，而A就能在2MSL时间内收到这个重传的FIN+ACK报文段。如果A在TIME-WAIT状态不等待一段时间，而是在发送完ACK报文段后就立即释放连接，就无法收到B重传的FIN+ACK报文段，因而也不会再发送一次确认报文段。这样，B就无法按照正常的步骤进入CLOSED状态

2. A在发送完ACK报文段后，再经过2MSL时间，就可以使本连接持续的时间所产生的所有报文段都从网络中消失。这样就可以使下一个新的连接中不会出现这种旧的连接请求的报文段。

相关链接: [https://www.cnblogs.com/wen-ge/articles/5819699.html](https://www.cnblogs.com/wen-ge/articles/5819699.html)

### 一次完整的HTTP请求过程是怎样的

1. 对www.baidu.com这个网址进行DNS域名解析，得到对应的IP地址

2. 根据这个IP，找到对应的服务器，发起TCP的三次握手

3. 建立TCP连接后发起HTTP请求

4. 服务器响应HTTP请求，浏览器得到html代码

5. 浏览器解析html代码，并请求html代码中的资源（如js、css图片等）（先得到html代码，才能去找这些资源）

6. 浏览器对页面进行渲染呈现给用户

相关链接: [https://www.cnblogs.com/xuzekun/p/7527736.html](https://www.cnblogs.com/xuzekun/p/7527736.html)

### HTTP2和HTTP的区别有哪些

1. HTTP/2采用二进制格式而非文本格式
2. HTTP/2是完全多路复用的，而非有序并阻塞的——只需一个连接即可实现并行
3. 使用报头压缩，HTTP/2降低了开销
4. HTTP/2让服务器可以将响应主动“推送”到客户端缓存中

相关链接: [https://blog.csdn.net/lijiawnen/article/details/97791458](https://blog.csdn.net/lijiawnen/article/details/97791458)

### 在你的项目中你使用过哪些设计模式? 主要用来解决什么问题

1. 工厂模式: Spring如何创建Bean 
   
   相关链接: [https://jingyan.baidu.com/article/3c343ff7a1805c0d37796319.html](https://jingyan.baidu.com/article/3c343ff7a1805c0d37796319.html)

2. 代理模式: Motan服务的动态代理 
   
   相关链接: [https://www.jianshu.com/p/f6a419db3411](https://www.jianshu.com/p/f6a419db3411)

3. 责任链模式: Netty消息处理的方式 
   
   相关链接: [https://blog.csdn.net/lionaiying/article/details/53915109](https://blog.csdn.net/lionaiying/article/details/53915109)

4. 适配器模式: SLF4J如何支持Log4J 
   
   相关链接: [https://www.jianshu.com/p/76e34982b002](https://www.jianshu.com/p/76e34982b002)

5. 观察者模式: GRPC是如何支持流式请求的 
   
   相关链接: [https://www.cnblogs.com/gutousu/p/9970288.html](https://www.cnblogs.com/gutousu/p/9970288.html)

6. 构造者模式: PB序列化中的Builder 
   
   相关链接: [https://www.cnblogs.com/mxz1994/p/9509980.html](https://www.cnblogs.com/mxz1994/p/9509980.html)

### Object中的equals和hashcode的作用分别是什么?

hashCode()方法和equal()方法的作用其实一样

equals和hashCode的区别: [https://www.cnblogs.com/keyi/p/7119825.html](https://www.cnblogs.com/keyi/p/7119825.html)

### final, finally, finalize的区别与使用场景

1. final : 可以作为修饰符修饰变量、方法和类

2. finally: 用在异常处理中定义总是执行代码 

3. finalize: 当垃圾回收器将无用对象从内存中清除时，该对象的finalize()方法被调用

相关链接: [https://jingyan.baidu.com/article/597a064363b676312b5243ad.html](https://jingyan.baidu.com/article/597a064363b676312b5243ad.html)

### 简单描述一下java的异常机制

java对异常进行了分类，不同类型的异常使用了不同的java类，所有异常的根类为java.lang.Throwable.Throwable派生了2个子类：Error和Exception.

1. Error表示程序本身无法克服和恢复的一种严重错误

2. Exception表示还能克服和恢复，其中又分为系统异常和普通异常
   
   1. 系统异常是软件本身缺陷导致的问题
   
   2. 普通异常是运行环境的变化或异常导致的问题

相关链接: [https://www.cnblogs.com/guweiwei/p/6611746.html](https://www.cnblogs.com/guweiwei/p/6611746.html)

### 线上使用的哪个jdk版本,为什么使用这个版本(有什么特点)

各个版本各不相同 可以参考: [https://segmentfault.com/a/1190000004419611](https://segmentfault.com/a/1190000004419611)

介绍了各个版本的新特性

## 深入浅出JVM <a name = 'jvm'> </a>

### 简单描述一下JVM的内存模型

#### JVM内存模型

根据JVM规范，JVM 内存共分为虚拟机栈，堆，方法区，程序计数器，本地方法栈五个部分。

相关链接: [https://blog.csdn.net/qzqanzc/article/details/81008598](https://blog.csdn.net/qzqanzc/article/details/81008598)

#### Java堆内存访问模型

相关链接: [https://blog.csdn.net/Mr__fang/article/details/90269029](https://blog.csdn.net/Mr__fang/article/details/90269029)

### 什么时候会触发FullGC?

年轻代晋升时,老年代空间不足,永久代不足等

相关链接: [https://www.jianshu.com/p/74b1f76834a1](https://www.jianshu.com/p/74b1f76834a1)

### Java类加载器有几种,关系怎样的?

1. 双亲委派机制

2. Bootstrap类加载器

3. Extension类加载器

4. System类加载器

5. 自定义类加载器

相关链接: [https://blog.csdn.net/qq_33583322/article/details/81239508](https://blog.csdn.net/qq_33583322/article/details/81239508)

### 双亲委派机制的加载流程是怎样的,有什么好处?

某个特定的类加载器在接到加载类的请求时，首先将加载任务委托给父类加载器，依次递归，如果父类加载器可以完成类加载任务，就成功返回；只有父类加载器无法完成此加载任务时，才自己去加载。

相关连接: [https://www.cnblogs.com/webster1/p/7977618.html](https://www.cnblogs.com/webster1/p/7977618.html)

### 1.8为什么用Metaspace替换掉PermGen?Meatspace保存在哪里?

1. 字符串存在永久代中，容易出现性能问题和内存溢出

2. 类及方法的信息等比较难确定其大小，因此对于永久代的大小指定比较困难，太小容易出现永久代溢出，太大则容易导致老年代溢出。

3. 永久代会为 GC 带来不必要的复杂度，并且回收效率偏低。

4. Oracle 可能会将HotSpot 与 JRockit 合二为一。

相关链接: [https://blog.csdn.net/lk7688535/article/details/51767460](https://blog.csdn.net/lk7688535/article/details/51767460)

### 编译器会对指令做哪些优化?(简答描述编译器的指令重排)

1. 公共子表达式的消除

2. 指令重排

3. 内联

4. 逃逸分析 
   
   a) 方法逃逸 
   
   b) 线程逃逸

5. 栈上分配

6. 同步消除

相关链接: [https://blog.csdn.net/u013305783/article/details/81279175](https://blog.csdn.net/u013305783/article/details/81279175)

指令重排 参考链接: [https://www.cnblogs.com/tuhooo/p/7921651.html](https://www.cnblogs.com/tuhooo/p/7921651.html)

### 简单描述一下volatile可以解决什么问题?如何做到的?

阻止指令的重排序 可以保证变量读写的有序性

相关链接: [https://www.cnblogs.com/dolphin0520/p/3920373.html](https://www.cnblogs.com/dolphin0520/p/3920373.html)

### 简单描述一下GC的分代回收?

分代回收基于两个事实:大部分对象很快就不使用了,还有一部分不会立即无用,但也不会持续很长时间.

相关链接: [https://blog.csdn.net/weixin_44060295/article/details/84958896](https://blog.csdn.net/weixin_44060295/article/details/84958896)

### G1与CMS的区别?

相关链接: [https://www.jianshu.com/p/bdd6f03923d1](https://www.jianshu.com/p/bdd6f03923d1)

### 对象引用有哪几种,有什么特点?

1. 强引用

2. 软引用

3. 弱引用

4. 虚引用

相关链接: [https://cloud.tencent.com/developer/article/1362804](https://cloud.tencent.com/developer/article/1362804)

### 使用过哪些JVM调试工具,主要分析哪些内容?

相关链接: [https://blog.csdn.net/yethyeth/article/details/73266455](https://blog.csdn.net/yethyeth/article/details/73266455)

> 以上题来自: 32个java面试必考点 拉勾网:  [https://kaiwu.lagou.com/course/courseInfo.htm?courseId=1](https://kaiwu.lagou.com/course/courseInfo.htm?courseId=1)
